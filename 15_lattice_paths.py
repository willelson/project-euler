import time

grid_size = 8
target = (grid_size, grid_size)

path_cache = {}


def paths_to_target(position):
    """ Return the number of paths to the target from a given postion

    For example, if the target is (2, 2) and position is (1, 1), there are two
    paths to the target - up then right or right then up, or if the target is
    (5, 5) and postion is (5, 2), there is one path to the target - up x 3
    """

    paths_from_position = 0
    x, y = position

    if x < grid_size:
        # If we're at the edge of the grid there is only one path available
        if y == grid_size:
            paths_from_position += 1
        else:
            adjacent_postition = (x + 1, y)
            paths_from_position += path_cache.get(adjacent_postition, 0)

    if y < grid_size:
        if x == grid_size:
            paths_from_position += 1
        else:
            adjacent_postition = (x, y + 1)
            paths_from_position += path_cache.get(adjacent_postition, 0)

    path_cache[position] = paths_from_position

    return paths_from_position


if __name__ == '__main__':
    # Solution to https://projecteuler.net/problem=15
    count = 0

    start = time.time()

    # Work from target back to origin to fill path cache
    for x in range(grid_size, -1, -1):
        for y in range(grid_size, -1, -1):
            count = paths_to_target((x, y))

    end = time.time()

    print(f'There are {count} paths to the target of ({target[0]}, {target[1]})')
    print(f'Calculated in {end - start:.5f}s')
