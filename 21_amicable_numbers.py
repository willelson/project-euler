# https://projecteuler.net/problem=21


def proper_divisors(n):
    """ Get the proper divisors of n - numbers less than n which divide evenly
    into n
    """

    if n < 2:
        return []

    if n == 2:
        return [1]

    divisors = [1, 2]

    for i in range(int(n / 2), 3, -1):
        if n % i == 0:
            divisors.append(i)

    return sorted(divisors)


if __name__ == '__main__':
    amicable_sum = 0
    amicables = set()

    for i in range(10000):
        divisor_sum = sum(proper_divisors(i))
        if sum(proper_divisors(divisor_sum)) == i and i != divisor_sum:
            amicables.add(i)
            amicables.add(divisor_sum)

    print(amicables)
    print(sum(amicables))
