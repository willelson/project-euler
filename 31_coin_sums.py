COINS = [100, 50, 20, 10, 5, 2, 1]


def combinations_for_coin(target, coin):
    """ Return all combinations that begin with the passed in coin and do not exceed the sum of the target

    >>> combinations_for_coin(8, 5)
    [[5, 2, 1], [5, 1, 1, 1]]

    """
    combos = []
    coin_index = COINS.index(coin)

    multiples_before_target_exceeded = int(target / coin)

    if coin == 1:
        return [[1] * multiples_before_target_exceeded]

    if multiples_before_target_exceeded >= 1:
        for i in range(multiples_before_target_exceeded):
            combo = [coin] * (i + 1)

            next_target = (target - sum(combo))

            if next_target > 0:

                for next_coin in COINS[coin_index + 1:]:
                    for next_combo in combinations_for_coin(
                        next_target, next_coin
                    ):
                        combos.append(combo + next_combo)

            else:
                combos.append(combo)
    return combos


def get_all_combinations(target):
    """ Get all combinations of making a target with the available coins

    >>> get_all_combinations(6)
    [[5, 1], [2, 2, 2], [2, 2, 1, 1], [2, 1, 1, 1, 1], [1, 1, 1, 1, 1, 1]]

    """
    combos = []
    available_coins = [coin for coin in COINS if coin <= target]

    for coin in available_coins:
        combos.extend(combinations_for_coin(target, coin))

    return combos


if __name__ == '__main__':
    all_combos = []
    target = 200

    all_combos = get_all_combinations(target)

    if target < 25:
        for combo in all_combos:
            if sum(combo) != target:
                print('ERROR')
            print(combo)

    print(f'There are {len(all_combos)} ways to make {target} from the coins {COINS}')
